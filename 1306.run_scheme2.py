
# coding: utf-8

__author__ = "Hiori Kino"
__date__ = "May 09, 2019"
__version__ = "May_09_2019_A"

# add KKR script path
import sys
#p = "/home/kino/kino/pythonscript/akaikkr/akaikkr_input_brvtyp2"
kkrscriptpath = "/home/kino/kino/work/fukushima_HEA/akaikkr_input_brvtyp2"
sys.path.append(kkrscriptpath)
from kkrinput_brvtyp import KKRinput_brv
from hea_util import *
from akaikkrio2 import OutputDOS, OutputGo, OutputJ

from HEARun import *
from HEAPathSearch import *

import os
import subprocess

import matplotlib.pyplot as plt
import numpy as np
from itertools import combinations
import pandas as pd
import glob
from IPython.display import display
import collections

import logging

os.environ["MKL_NUM_THREADS"]="1"


# In[2]:

def headirname2prop(dir_):
    s = dir_.split(",")
    dic = collections.OrderedDict()
    for q in s:
        ss = q.split("_")
        dic[ss[0]] =ss[1]
    return dic


# In[3]:

def get_uniqkeylist(prefix="RUN"):
    keylist = []
    for dir_ in glob.glob(os.path.join(prefix,"key_*")):
        headirname = os.path.split(dir_)[-1]
        prop = headirname2prop(headirname)
        keylist.append(prop["key"])
    keylist = list(set(keylist))
    return keylist




def calculate_pm(key,ew,ed,polytyp,ipm,param,only_once=False):
    param0 = copy.deepcopy(param)
    prefix = param["prefix"]
    iew,ewidth = ew[0],ew[1]
    ied,edelt = ed[0],ed[1]

    # bug fix 
    param0["edelt"] = edelt 

    copyfiles_from = None
    #dir_first = None
    if ipm >0:
        # search the last pm
        p = {"key":key,"ew":"{:03}".format(iew),"ed":"{:03}".format(ied),
                         "polytyp":polytyp,"pm":"{:03}".format(ipm-1) }
        HEAPathSearch_new = HEAPathSearch(prefix,p)
        dir_ = os.path.join(prefix,HEAPathSearch_new.dirname)
        copyfiles_from = dir_ 

        # search the first pm
        #p = {"key":key,"ew":"{:03}".format(iew),"ed":"{:03}".format(ied),
        #                 "polytyp":polytyp,"pm":"{:03}".format(0) }
        #HEAPathSearch_first = HEAPathSearch(prefix,p)
        #dir_first = os.path.join(prefix,HEAPathSearch_first.dirname)
        #copyfiles_from = dir_first


    for pm in [1e-2, 5e-3, 1e-3, 5e-4,1e-4]:
        param = copy.deepcopy(param0)
        print("pm={}".format(pm))
        for it in range(20):
            print("it={}".format(it))
            p = {"key":key,"ew":"{:03}".format(iew),"ed":"{:03}".format(ied),
                 "polytyp":polytyp,"pm":"{:03}".format(ipm) }

            HEAPathSearch_new = HEAPathSearch(prefix,p)
            dir_ = os.path.join(prefix,HEAPathSearch_new.dirname)
            print("directoty",dir_,"ewidth,edelt,pm",ewidth,edelt,pm) 

            if ipm == 0: 
                param.update({"maxitr":param["maxitr_init"],"pmix":param["pmix_init"]}) 
            else:
                param.update({"maxitr":300,"pmix":pm})
            hearun = HEArun(polytyp,key,ewidth,param)
            ipm = ipm +1
            
            print("copyfiles_from",copyfiles_from)
            converged = hearun.run_all(dir_,copyfiles_from=copyfiles_from)

            filename = os.path.join(os.path.join(prefix,HEAPathSearch_new.dirname),param["out_dos_log"])
            dossum = None 
            if os.path.exists( filename):
                outputdos = OutputDOS(filename)
                dossum = outputdos.get("totaldos")

            if only_once:
                return converged,dossum

            if converged:
                return True,dossum # converged
            
            output = OutputGo( os.path.join(dir_,param["out_go_log"]),
                              heakey=key, action=["history"])            
            
            if output.converging():
                print("converging, continue")
                copyfiles_from = dir_
            else:
                print("not converging, break")
                #if dir_first is not None:
                #    copyfiles_from = dir_first
                break # use the next pm value   
                
    return False,dossum 

def scf_one(key,ew,ed,polytyplist,ipm,param, only_once=False ):
    print(key,ew,ed,ipm,polytyplist)
    iew,ewidth = ew
    ied,edelt = ed 
    # ipm is index to pm

    converged = {}
    dossumdic = {}

    for polytyp in polytyplist:
        converged[polytyp],dossumdic[polytyp] = calculate_pm(key,ew,ed,polytyp,ipm,param,only_once=only_once)

    return converged,dossumdic

def make_new_ewidth(oldewidth,converged,dossumdic,param):

    print("converged? ",converged)
    print("calculated?",[ dossumdic[d] is not None for d in dossumdic])
    analyze_dos = True
    for values in dossumdic.values():
       if values is  None:
           analyze_dos = False
    if not analyze_dos:
        print("inconsistent abort")
        raise 

    convergedvalues = np.array(list(converged.values()))
    keyconverged = False

    dossumlist = []
    for polytyp in polytyplist:
        dossumlist.append( dossumdic[polytyp])
    analyzedos = analyzeDOS( dossumlist,param["dosth"])
    msg = analyzedos.calc_new_ewidth(oldewidth)
    print(msg, "bcc_fcc_converged?",(convergedvalues==True).all() )
    if msg[0] =="old" and (convergedvalues==True).all():
        print("use_old_ewidth and converged, FIN")
        keyconverged = True

    if msg[0]=="fail":
        print("warning, in make_new_ewidth")

    return keyconverged,msg[0],msg[1],msg[2]


def main():

    logging.basicConfig(filename='_1305.run_scheme2.warning.log', level=logging.DEBUG)


    param0 = { # execution environment
            "specx":  "/home/kino/kino/work/fukushima_HEA/akaikkr_jij_sic_true/specx",
            "prefix": "RUN4again",
            "out_go_log": "out_go.log",  "out_dos_log": "out_dos.log", "out_j_log": "out_j.log",
            "inputcard_go": "inputcard_go",   "inputcard_dos": "inputcard_dos",  "inputcard_j": "inputcard_j", 
            # akaikkr keywords
            "file": "pot.dat",
            "bzqlty":2 , "maxitr":500 , "maxitr_init":500, "maxitr_2nd": 200,"pmix_init": 0.005, 
            "dosth": 1e-2,  "ewidth_init": 1.2,  "ewidth_dos": 3.0, 
            # plot setting
            "plot_history": False, "plot_dos": False, "plot_cdos":False       }

    param = copy.deepcopy(param0)


#    keylist = [13142224,13142348,13142349,13142350,13142441,13142442,13142443,13142474,13142475 ]
    keylist = [13142349]
    keylist = list(map(str,keylist))

    polytyplist = ["bcc","fcc"]

    if not os.path.exists(param["prefix"]):
        os.mkdir(param["prefix"])
                 
    for key in keylist:
        print("==============",key)

        ewidth_tried = []
        ewidth_candidates = [ param["ewidth_init"] ]

        #ewidth_candidates = [ 1.2, 1.0, 1.4, 1.6 ] # <---- possible way, but not yet coded.

        iew = -1
        #for iew in range(10):
        while iew<10:


            if iew == -1:
                iew += 1
                ewidth = ewidth_candidates.pop(0) # must have it
#                param["maxitr"] = param["maxitr_init"]
#                param["pmix"] = param["pmix_init"]
            else:
                print("ewidth_candidates",ewidth_candidates)
                while True:
                    ewidth = None
                    if len(ewidth_candidates) >0:
                        iew += 1
                        ewidth_tmp = ewidth_candidates.pop(0)
                        if ewidth_tmp in ewidth_tried:
                            continue
                        else:
                            ewidth = ewidth_tmp
                            break
                    else:
                        break 

            if ewidth is None:
                print("not converged with all the plans")
                break

            # make_newewidth() set ewidth for iew>0

            ied = 0
            edlet = 1e-4
            ipm = 0

            print("-------------STEP1",key,"roughly estimate valid ewidth")
            while True:
                converged,dossumdic = scf_one(key,[iew,ewidth],[ied,edlet],polytyplist,ipm,param,only_once=True)

                ewidth_tried.append(ewidth)
                keyconverged,nextplan,ewidth,ewidth_candidates = make_new_ewidth(ewidth,converged,dossumdic,param)
                if keyconverged:
                    break 
                if nextplan=="fail":
                    log_msg = "Failed to have a new ewidth. step1, key={}, iew={}, but continue.".format(key,iew)
                    print(log_msg)
                    logging.warning(log_msg)
                    keyconverged = True
                    break 
                if nextplan=="old":
                    ipm += 1
                    break
                #iew += 1
                # use new ewidth given by  make_new_ewidth

            if keyconverged:
                break 

            print("-------------STEP2",key,"tightly achieve SCF")
            param = copy.deepcopy(param0)
            param["maxitr"] = param["maxitr_2nd"]
            # change or not change ewidth
            for ied, edelt in enumerate([1e-4,1e-3,1e-2]):

                converged,dossumdic = scf_one(key,[iew,ewidth],[ied,edelt],polytyplist,ipm,param)
            
                ewidth_tried.append(ewidth)
                keyconverged,nextplan,ewidth,ewidth_candidates = make_new_ewidth(ewidth,converged,dossumdic,param)

                if keyconverged:
                    break  # ied and iew loop 
                if nextplan=="fail":
                    log_msg = "Failed to have a new ewidth. step2, key={}, iew={}, ied={}, but continue.".format(key,iew,ied)
                    print(log_msg)
                    logging.warning(log_msg)
                    keyconverged = True
                    break 
                if nextplan =="new":
                    #iew += 1
                    break  # ied loop 

                # reset ipm
                ipm = 0 
#                param["maxitr"] = param["maxitr_init"]
#                param["pmix"] = param["pmix_init"]

            if keyconverged:
                break  # iew loop 
            if nextplan == "old":
                print("USE alternative ewidth candidates",ewidth_candidates)
                ewidth_candidates = list(ewidth_candidates)

        #for iew in range(10):
        if not keyconverged:
            print("not converged with all the plans")
            
main()

