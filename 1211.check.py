
# coding: utf-8

# In[1]:


# add KKR script path
import sys
#p = "/home/kino/kino/pythonscript/akaikkr/akaikkr_input_brvtyp2"
p = "/home/kino/kino/work/fukushima_HEA/akaikkr_input_brvtyp2"
sys.path.append(p)
from kkrinput_brvtyp import KKRinput_brv
from hea_util import *
from akaikkrio2 import OutputDOS, OutputGo, OutputJ

from HEARun import *
from HEAPathSearch import *

import os
import subprocess

import matplotlib.pyplot as plt
import numpy as np
from itertools import combinations
import pandas as pd
import glob
from IPython.display import display
import collections

os.environ["MKL_NUM_THREADS"]="1"


# In[2]:

def headirname2prop(dir_):
    s = dir_.split(",")
    dic = collections.OrderedDict()
    for q in s:
        ss = q.split("_")
        dic[ss[0]] =ss[1]
    return dic


# In[3]:

def get_uniqkeylist(prefix="RUN"):
    keylist = []
    for dir_ in glob.glob(os.path.join(prefix,"key_*")):
        headirname = os.path.split(dir_)[-1]
        prop = headirname2prop(headirname)
        keylist.append(prop["key"])
    keylist = list(set(keylist))
    return keylist


# In[4]:

def make_dfresult(keylist,param):
    prefix = param["prefix"]
    
    resultsummary = []
    result = []
    for ikey,key in enumerate(keylist):
        print(key,heakey2elements(key))
        keyconverged = False
        

        ewlist = []
        heaserach_ew = HEAPathSearch(prefix,{"key":key}).find()
        
        for ew in heaserach_ew.targetvalues:
            
            HEAPathSearch_ed = HEAPathSearch(prefix,{"key":key,"ew":ew}).find()
            for ed in HEAPathSearch_ed.targetvalues:
                
                print("try ew={}, ed={}".format(ew,ed))
                dossumdic = {}
                converged = {}
                for polytyp in polytyplist:
                    dossumdic[polytyp] = None
                    converged[polytyp] = False
                
                for polytyp in polytyplist:

                    HEAPathSearch_pm = HEAPathSearch(prefix,{"key":key,"ew":ew,"ed":ed, 
                                                             "polytyp":polytyp}).find()
                    for pm in HEAPathSearch_pm.targetvalues:

                        p = {"key":key,"ew":ew,"ed":ed,"polytyp":polytyp,HEAPathSearch_pm.targetkey:pm}

                        HEAPathSearch_new = HEAPathSearch(prefix,p)
                        print(os.path.join(prefix,HEAPathSearch_new.dirname))
                        filename = os.path.join(os.path.join(prefix,HEAPathSearch_new.dirname),param["out_go_log"])
                        output = OutputGo(filename,action=["history"])
                        
                        if param["plot_history"]:
                            output.plot("all")

                        converging = output.converging()
                        
                        if True:
                            print(
                                  key,ew,polytyp,ed,pm,
                                  "ewidth=",output.dic["ewidth"],               
                                  "edelt=",output.dic["edelt"],                                
                                  "pmix=",output.dic["pmix"],
                                  "maxitr=",output.dic["maxitr"],
                                  converging,
                                  output.dic["converged"]) 
                        result.append( [HEAPathSearch_new.dirname,
                                key,ew,polytyp,ed,pm,
                                output.dic["ewidth"],
                                output.dic["edelt"],
                                output.dic["pmix"],
                                output.dic["maxitr"],
                                output.dic["h_err"][-1],
                                converging,
                                output.dic["converged"] ] )
                        label = ["dirname", "key","ew","polytyp","ed","pm", 
                                 "ewidth", "edelt", "pmix", "maxitr","h_err","converging","converged"]
                        
                        converged[polytyp] = output.dic["converged"] 

                        filename = os.path.join(os.path.join(prefix,HEAPathSearch_new.dirname),
                                                param["out_dos_log"])
                        if os.path.exists( filename):
                            outputdos = OutputDOS(filename)
                            if param["plot_dos"]:
                                outputdos.plot("totaldos")
                            if param["plot_cdos"]:
                                outputdos.plot("componentdos")
                            dossumdic[polytyp] = outputdos.get("totaldos")
                            print("debug, dossumdic, polytyp")
                            print(dossumdic[polytyp][:5,:])
                            print("set_value_in_dossumdic_for_polytyp",polytyp)

                print("\ncheckDOS, key {} ew={}".format(key,ew))
                analyze_dos_again = True
                for values in dossumdic.values():
                    if values is  None:
                        analyze_dos_again = False

                print("converged? ",converged)
                print("calculated?",[ dossumdic[d] is not None for d in dossumdic])

                convergedvalues = np.array(list(converged.values()))

                if analyze_dos_again:
                    dossumlist = []
                    for polytyp in polytyplist:
                        dossumlist.append( dossumdic[polytyp])

                    analyzedos = analyzeDOS( dossumlist,param["dosth"])
                    msg = analyzedos.calc_new_ewidth(output.dic["ewidth"])
                    print(msg, "bcc_fcc_converged?",(convergedvalues==True).all() ) 

                    if msg[0] =="old" and (convergedvalues==True).all():
                        print("use_old_ewidth and converged, FIN")
                        keyconverged = True

                if keyconverged:
                    print("try next key\n")
                    break
                else:
                    print("NOT CONVERGED, key=",key)

                print()

        resultsummary.append([key,keyconverged])
        
    return resultsummary, result, label


# In[5]:

param= { "specx":  "/home/kino/kino/work/fukushima_HEA/akaikkr_jij_sic_true/specx",
        "prefix": "RUN2",
        "out_go_log": "out_go.log",  "out_dos_log": "out_dos.log", "out_j_log": "out_j.log",
        "inputcard_go": "inputcard_go",   "inputcard_dos": "inputcard_dos",  "inputcard_j": "inputcard_j", 
        "bzqlty":10 , "maxitr":500 ,        "dosth": 1e-2,    "ewidth_dos": 3.0, 
        "plot_history": False, "plot_dos": False, "plot_cdos":False       }


# In[6]:

keylist =  get_uniqkeylist()
resultsummary, result,label = make_dfresult(keylist,param)


# In[11]:

df_result = pd.DataFrame(result,columns=label)
df_result.iloc[:,1:]
df_result.to_csv("result.csv")

# In[8]:

df_summary = pd.DataFrame(resultsummary,columns=["key","conveged"])
print(df_summary.query("conveged==False"))
df_summary.to_csv("summary.csv")


keylist = [13142475]
keylist = list(map(str,keylist))

param= { "specx":  "/home/kino/kino/work/fukushima_HEA/akaikkr_jij_sic_true/specx",
        "prefix": "RUN2",
        "out_go_log": "out_go.log",  "out_dos_log": "out_dos.log", "out_j_log": "out_j.log",
        "inputcard_go": "inputcard_go",   "inputcard_dos": "inputcard_dos",  "inputcard_j": "inputcard_j", 
        "bzqlty":10 , "maxitr":500 ,        "dosth": 1e-2,    
        "plot_history":True, "plot_dos": True, "plot_cdos":False       }

summary1,result1,label1 = make_dfresult(keylist,param)



df_result1 = pd.DataFrame(result1,columns=label1)
df_result1.to_csv("result1.csv")



# In[ ]:



# In[ ]:




# In[ ]:



